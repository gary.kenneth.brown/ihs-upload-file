const AWS = require('aws-sdk');

const s3Bucket = new AWS.S3();
const dynamoDb = new AWS.DynamoDB.DocumentClient();

const tableName = "ihs-evidence";

exports.handler = async (event) => {
    
    var reference = event.reference;
    var filename = event.body.filename;
    var ctype = event.body.contentType;
    var fileContents = Buffer.from(event.body.data, 'base64');
    
    var s3data = {
      Key: reference + "/" + filename,
      Body: fileContents,
      Bucket: process.env.BUCKET_NAME,
      ContentType: ctype
    };
    
    s3Bucket.putObject(s3data, function(err, data) {
        if (err) {
          console.log(err);
          console.log('Error uploading data: ', data);
        } else {
          console.log('Data successfully uploaded');
        }
    });
    
    //
    // lookup existing record
    //
    const params = { 
        TableName: tableName,
        ExpressionAttributeValues: {
            ':ref': reference
        },
        KeyConditionExpression: 'bsaReference = :ref'
    };
    
    var records = await dynamoDb.query(params).promise();
    var evidenceRecord = records.Items[0];
    
    // existing
    if(evidenceRecord) {
        evidenceRecord.files.push({
            filename: filename,
            fileId: "uuid",
            documentType: "Payslip Claim Evidence",
            addedDate: Date.now(),
            contentType: ctype
        });
    }
    else {
        // new record
        evidenceRecord = {
            bsaReference: reference,
            expirationTime: Math.round(new Date().getTime()/1000) + 6000,
            files: [{
                filename: filename,
                fileId: "uuid",
                documentType: "Payslip Claim Evidence",
                addedDate: Date.now(),
                contentType: ctype
            }]
        };
    }
    
    //
    // Save the record new or updated
    //
    await dynamoDb.put({
        TableName: tableName,
        Item: evidenceRecord
    }).promise();
    
    return {
        statusCode: 200,
        body: 'file uploaded',
    };
};
